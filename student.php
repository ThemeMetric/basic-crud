<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'db.php';
$query = "select * from students";
$result = $pdo->query($query);
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Crud</title>
    
    <style type="text/css"> 
    
        table{
            
            border-left: 1px solid #000;
        border-top: 1px solid #000;
        }
           table td, table th {
        border-right: 1px solid #000;
        border-bottom: 1px solid #000;
        padding: 10px;
    }
    
    </style>
</head>
<body>
    
   <form method="post" action="uploadfile.php" enctype="multipart/form-data">
        
        <input type="file" name="file" /> <button>Upload csv</button>
        
        
    </form>
    
    
    
    <?php

if(isset($_SESSION['message'])) {
    echo "<b>".$_SESSION['message']."</b>";
    unset($_SESSION['message']);
}
?>
    <br>
    
    <table border="0" cellspacing="0">
    
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Name
            </th>
            <th>
                Phone
            </th>
            <th>
                Email
            </th>
            <th>
                Course
            </th>
            <th>
                Actions
            </th>
            
        </tr>
    </thead>
     <tbody>
    
    <?php 
    $i=1;
    while ($row = $result->fetchObject()){   
    ?>
    
    <tr> 
    <td><?php echo $i++?> . </td>
    <td><?php echo $row->name?> </td>
    <td><?php echo $row->phone?> </td>
    <td><?php echo $row->email?> </td>
    <td><?php echo $row->course?> </td>
    <td>
        <a href="view.php?id=<?php echo $row->id?>" >View</a> 
        <a href="edit.php?id=<?php echo $row->id ?>">Edit</a> 
        <a href="delete.php?id=<?php echo $row->id ?>">Delete</a> 
    
    
    </td>
    
    
    </tr>
    
    <?php 
    }
    ?>

    </tbody>
</table>  
    
    <a href="student.html">Add New student</a>
</body>
</html>
