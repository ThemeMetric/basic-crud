-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2016 at 11:51 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `batch_three`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `phone`, `email`, `course`) VALUES
(1, 'Sajjad', '01788788920', 'sajjad4930@gmail.com', 'Basic-php voda'),
(2, 'Akash', '0178895200', 'akash@gmail.com', 'Php'),
(4, 'Maria', '0159875522', 'maria@gmail.com', 'html css'),
(7, 'mama', '6', 'mmama@gmail.com', ''),
(8, 'fdhdfh', 'sdgfsdfgsd', 'sdgsdf@fffff', 'dsgdsfgsd'),
(10, 'sdgsdg', 'sdgsd', 'sdgsd', 'sdgsd'),
(11, 'sdgsd', 'SGsdfgsg', 'thdghfd', 'fgsdfg'),
(12, 'sgsdgsd', 'sdgsdfg', 'sdgsdfg', 'sdgsdfgs'),
(13, 'dsgsdGsdg', 'sdgsdg', 'gsdghsdfhgsdg', 'sdgsdgsd'),
(14, 'dsgsdGsdg', 'sdgsdg', 'gsdghsdfhgsdg', 'sdgsdgsd'),
(15, 'sdgsd', 'SGsdfgsg', 'thdghfd', 'fgsdfg'),
(16, 'fdhdfh', 'sdgfsdfgsd', 'sdgsdf@fffff', 'dsgdsfgsd'),
(17, 'dsgsdGsdg', 'sdgsdg', 'gsdghsdfhgsdg', 'sdgsdgsd'),
(19, 'Sajjad', '01788788920', 'sajjad4930@gmail.com', 'Basic-php voda'),
(20, 'sdgsd', 'SGsdfgsg', 'thdghfd', 'fgsdfg'),
(21, 'sdgsd', 'SGsdfgsg', 'thdghfd', 'fgsdfg'),
(22, 'sdgsd', 'SGsdfgsg', 'thdghfd', 'fgsdfg'),
(23, 'Akash', '0178895200', 'akash@gmail.com', 'Php'),
(27, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(28, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(29, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(30, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(31, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(32, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(33, 'mizan', '123456', 'hhh@gmail.com', 'hfdfdj'),
(34, 'Sajjaddsfgsdfd', '564', 'a2gmail.com', 'asfdsaf'),
(35, 'Sajjad', '01788788920', 'sajjad4930@gmail.com', 'HTML');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
